'use strict';
let up_1 = document.querySelector('button#up_1');
let up_2 = document.querySelector('button#up_2');
let reset = document.querySelector('button#reset');
let counter = document.querySelector('p#counter');
let congrats = document.querySelector('p#congrats');

// Initial fetch total number of clicks from db on load
fetch('clicks', { method: 'GET' })
  .then(res => {
    if (!res.ok) {
      console.log(res);
      throw new Error('Trouble getting count from /clicks endpoint')
    }
    return res.json();
  })
  .then(res => {
    counter.textContent = res.totalCount;
  })
  .catch(err => console.log(err))

up_1.addEventListener('click', event => {
  event.preventDefault();
  postClicks({ 'numClicks': 1 });
});

up_2.addEventListener('click', event => {
  event.preventDefault();
  postClicks({ 'numClicks': 2 });
})

reset.addEventListener('click', event => {
  event.preventDefault();
  fetch('clicks', { method: 'POST'})
    .then(res => {
      if (!res.ok) {
        throw new Error('Reset did not take.');
      }
      return res.json();
    })
    .then(res => {
      counter.textContent = res.totalCount;
      congrats.textContent = '';
    })
    .catch(err => console.log(err))
})


function cheer() {
  if (Number(counter.textContent) >= 150) {
    congrats.textContent = 'You did it! Cha-ching!'
  }
}

// function to post clicks to the database
function postClicks(clickData = {}){
  return fetch('up', {
    method: 'POST',
    headers: {
      'Content-type': 'application/json'
    },
    body: JSON.stringify(clickData)
  })
    .then(res => {
      if (!res.ok) {
        throw new Error('Click request failed');
      }
      return res.json();
    })
    .then(res => {
      counter.textContent = res.totalCount;
      cheer();
    })
    .catch(err => console.log(err));
}
