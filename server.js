'use strict';
const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
require('dotenv').config();

// Connect to MongoDB
let uri = process.env.MONGO_URI;
mongoose.connect(uri, { useNewUrlParser: true });

// MongoDB schema
var counterSchema = new mongoose.Schema({
  totalCount: {
    type: Number,
    required: true
  },
  tally: {
    type: Number,
    required: true
  },
  timeAdded: {
    type: Date,
    default: new Date()
  }
})
var Counter = mongoose.model('Counter', counterSchema);

// CRUD functions for working with Counters collection in MongoDB
var getLastCount = function(done){
  Counter.findOne()
    .sort({ timeAdded: 'desc' })
    .exec((err, lastCount) => {
      if (err) {
        done(err);
      } else {
        done(null, lastCount);
      }
    })
}

var addToCount = function(num, done){
  getLastCount((err, prevCount) => {
    if (err) {
      console.log(err);
    }
    var newCount;
    // if the DB is empty of documents, the passed-in number is the total count
    //  else add the passed-in number to the total count
    if (!prevCount) {
      newCount = num;
    } else {
      newCount = prevCount.totalCount + num;
    }
    // Create a new document for the DB
    var updatedCount = new Counter({
      totalCount: newCount,
      tally: num,
      timeAdded: new Date()
    });
    // Add the new document to the DB
    updatedCount.save((err, newTotal) => {
      if (err) done(err);
      else done(null, newTotal);
    })
  })
}

var resetCount = function(done) {
  getLastCount((err, prevCount) => {
    if (err) {
      console.log(err);
    }
    var newCount;
    // if the DB is empty of documents, just set the new total to 0
    //  else subtract 150 from the total count
    if (!prevCount) {
      newCount = 0;
    } else {
      let diff = Number(prevCount.totalCount) - 150;
      newCount = Math.max(diff, 0);
    }
    // Create a new document for the DB
    var updatedCount = new Counter({
      totalCount: newCount,
      tally: 0,
      timeAdded: new Date()
    });
    // Add the new document to the DB
    updatedCount.save((err, newTotal) => {
      if (err) done(err);
      else done(null, newTotal);
    })
  })
}

// server functions
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static('public'));

io.on('connection', (socket) => {

  socket.on('get clicks', (fn) => {
    getLastCount((err, prevCount) => {
      if (err) throw Error(err);
      if (!prevCount) fn(0);
      else return fn(prevCount.totalCount);
    });
  });

  socket.on('update clicks', (numClicks) => {
    addToCount(numClicks, (err, _newCount) => {
      if (err) console.log(err);
      io.emit('update clicks', _newCount);
    })
  });

  socket.on('reset clicks', (fn) => {
    resetCount((err, newCount) => {
      if (err) throw Error(err);
      return fn(newCount.totalCount);
    })
  })
})

// Add number of clicks to DB and return the new total count of clicks
app.post('/up', (req, res) => {
  var num2Add = req.body.numClicks;
  addToCount(num2Add, (err, _newCount) => {
    if (err) console.log(err);
    else res.send(_newCount);
  })
})

// Get total clicks
app.get('/clicks', (req, res) => {
  getLastCount((err, prevCount) => {
    if (err) {
      console.log(err);
    }
    if (!prevCount){
      res.send({ totalCount: 0 });
    } else {
      res.send(prevCount);
    }
  })
})

// Reset total clicks (subtract 150) and return updated total clicks
app.post('/clicks', (req, res) => {
  resetCount((err, newCount) => {
    if (err) {
      console.log(err);
    }
    if (!newCount) {
      res.send({ totalCount: 0 });
    } else {
      res.send(newCount);
    }
  })
})

const listener = server.listen(process.env.PORT || 3000, () => {
  console.log('Listening on port ' + listener.address().port);
})
