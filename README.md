# TallyUp

A counter for tallying up son's personal achievements
_(or a project to get some more practice using Node.js, Express, and JavaScript and to learn some MongoDB along the way)_

The goal:
My wife and I want to be able to track the number of points our son gets for doing certain things. We'd like to be able to add points from both of our phones (and maybe even just from our laptops or tablets) and have the total number of points updated in all places. Our son gets 1 point if he does it after we ask him, but he can get 2 points if he does it without needing to be asked. When he gets 150 points, he gets paid cash money, so we want to show a congratulatory note at that time. Then we can either keep tallying points or hit _reset_ to subtract 150 points from the total.

The tally of points is stored in MongoDB with the following schema:
```
totalCount: {
  type: Number,
  required: true
},
tally: {
  type: Number,
  required: true
},
timeAdded: {
  type: Date,
  default: new Date()
}
```
_tally_ is either 1 or 2 based on how many points were just awarded.
